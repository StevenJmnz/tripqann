/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Horario;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author StevenJmnz
 */
@Entity
@Table(name = "horarios")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Horarios.findAll", query = "SELECT h FROM Horarios h"),
    @NamedQuery(name = "Horarios.findById", query = "SELECT h FROM Horarios h WHERE h.id = :id"),
    @NamedQuery(name = "Horarios.findByCodigo", query = "SELECT h FROM Horarios h WHERE h.codigo = :codigo"),
    @NamedQuery(name = "Horarios.findByNumeroBus", query = "SELECT h FROM Horarios h WHERE h.numeroBus = :numeroBus"),
    @NamedQuery(name = "Horarios.findByDescripcion", query = "SELECT h FROM Horarios h WHERE h.descripcion = :descripcion"),
    @NamedQuery(name = "Horarios.findByTSalida", query = "SELECT h FROM Horarios h WHERE h.tSalida = :tSalida"),
    @NamedQuery(name = "Horarios.findByTLlegada", query = "SELECT h FROM Horarios h WHERE h.tLlegada = :tLlegada"),
    @NamedQuery(name = "Horarios.findByEstado", query = "SELECT h FROM Horarios h WHERE h.estado = :estado")})
public class Horarios implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "codigo")
    private String codigo;
    @Basic(optional = false)
    @Column(name = "numeroBus")
    private int numeroBus;
    @Basic(optional = false)
    @Column(name = "descripcion")
    private String descripcion;
    @Basic(optional = false)
    @Column(name = "tSalida")
    private String tSalida;
    @Basic(optional = false)
    @Column(name = "tLlegada")
    private String tLlegada;
    @Column(name = "estado")
    private Boolean estado;

    public Horarios() {
    }

    public Horarios(Integer id) {
        this.id = id;
    }

    public Horarios(Integer id, String codigo, int numeroBus, String descripcion, String tSalida, String tLlegada) {
        this.id = id;
        this.codigo = codigo;
        this.numeroBus = numeroBus;
        this.descripcion = descripcion;
        this.tSalida = tSalida;
        this.tLlegada = tLlegada;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public int getNumeroBus() {
        return numeroBus;
    }

    public void setNumeroBus(int numeroBus) {
        this.numeroBus = numeroBus;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getTSalida() {
        return tSalida;
    }

    public void setTSalida(String tSalida) {
        this.tSalida = tSalida;
    }

    public String getTLlegada() {
        return tLlegada;
    }

    public void setTLlegada(String tLlegada) {
        this.tLlegada = tLlegada;
    }

    public Boolean getEstado() {
        return estado;
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Horarios)) {
            return false;
        }
        Horarios other = (Horarios) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Horario.Horarios[ id=" + id + " ]";
    }
    
}
