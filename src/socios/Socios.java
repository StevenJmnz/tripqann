/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package socios;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author StevenJmnz
 */
@Entity
@Table(name = "socios")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Socios.findAll", query = "SELECT s FROM Socios s"),
    @NamedQuery(name = "Socios.findById", query = "SELECT s FROM Socios s WHERE s.id = :id"),
    @NamedQuery(name = "Socios.findByCodigo", query = "SELECT s FROM Socios s WHERE s.codigo = :codigo"),
    @NamedQuery(name = "Socios.findByNombre", query = "SELECT s FROM Socios s WHERE s.nombre = :nombre"),
    @NamedQuery(name = "Socios.findByApellidos", query = "SELECT s FROM Socios s WHERE s.apellidos = :apellidos"),
    @NamedQuery(name = "Socios.findByLineaSocio", query = "SELECT s FROM Socios s WHERE s.lineaSocio = :lineaSocio"),
    @NamedQuery(name = "Socios.findByNumeroBus", query = "SELECT s FROM Socios s WHERE s.numeroBus = :numeroBus"),
    @NamedQuery(name = "Socios.findByCapacidad", query = "SELECT s FROM Socios s WHERE s.capacidad = :capacidad"),
    @NamedQuery(name = "Socios.findByAyudante", query = "SELECT s FROM Socios s WHERE s.ayudante = :ayudante"),
    @NamedQuery(name = "Socios.findByChofer", query = "SELECT s FROM Socios s WHERE s.chofer = :chofer"),
    @NamedQuery(name = "Socios.findByEstado", query = "SELECT s FROM Socios s WHERE s.estado = :estado")})
public class Socios implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "codigo")
    private String codigo;
    @Basic(optional = false)
    @Column(name = "nombre")
    private String nombre;
    @Basic(optional = false)
    @Column(name = "apellidos")
    private String apellidos;
    @Basic(optional = false)
    @Column(name = "lineaSocio")
    private String lineaSocio;
    @Basic(optional = false)
    @Column(name = "numeroBus")
    private int numeroBus;
    @Basic(optional = false)
    @Column(name = "capacidad")
    private int capacidad;
    @Basic(optional = false)
    @Column(name = "ayudante")
    private String ayudante;
    @Basic(optional = false)
    @Column(name = "chofer")
    private String chofer;
    @Column(name = "estado")
    private Boolean estado;

    public Socios() {
    }

    public Socios(Integer id) {
        this.id = id;
    }

    public Socios(Integer id, String codigo, String nombre, String apellidos, String lineaSocio, int numeroBus, int capacidad, String ayudante, String chofer) {
        this.id = id;
        this.codigo = codigo;
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.lineaSocio = lineaSocio;
        this.numeroBus = numeroBus;
        this.capacidad = capacidad;
        this.ayudante = ayudante;
        this.chofer = chofer;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getLineaSocio() {
        return lineaSocio;
    }

    public void setLineaSocio(String lineaSocio) {
        this.lineaSocio = lineaSocio;
    }

    public int getNumeroBus() {
        return numeroBus;
    }

    public void setNumeroBus(int numeroBus) {
        this.numeroBus = numeroBus;
    }

    public int getCapacidad() {
        return capacidad;
    }

    public void setCapacidad(int capacidad) {
        this.capacidad = capacidad;
    }

    public String getAyudante() {
        return ayudante;
    }

    public void setAyudante(String ayudante) {
        this.ayudante = ayudante;
    }

    public String getChofer() {
        return chofer;
    }

    public void setChofer(String chofer) {
        this.chofer = chofer;
    }

    public Boolean getEstado() {
        return estado;
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Socios)) {
            return false;
        }
        Socios other = (Socios) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "socios.Socios[ id=" + id + " ]";
    }
    
}
