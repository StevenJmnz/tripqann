/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import controlador.exceptions.NonexistentEntityException;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import recorrido.Recorridos;

/**
 *
 * @author StevenJmnz
 */
public class RecorridosJpaController implements Serializable {

    public RecorridosJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
     private EntityManagerFactory emf = Persistence.createEntityManagerFactory("QANPU");

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public RecorridosJpaController() {
    }
    

    public void create(Recorridos recorridos) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(recorridos);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Recorridos recorridos) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            recorridos = em.merge(recorridos);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = recorridos.getId();
                if (findRecorridos(id) == null) {
                    throw new NonexistentEntityException("The recorridos with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Recorridos recorridos;
            try {
                recorridos = em.getReference(Recorridos.class, id);
                recorridos.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The recorridos with id " + id + " no longer exists.", enfe);
            }
            em.remove(recorridos);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }
    public void findbyid(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Recorridos recorridos;
            try {
                recorridos = em.getReference(Recorridos.class, id);
                recorridos.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The recorridos with id " + id + " no longer exists.", enfe);
            }
            
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Recorridos> findRecorridosEntities() {
        return findRecorridosEntities(true, -1, -1);
    }

    public List<Recorridos> findRecorridosEntities(int maxResults, int firstResult) {
        return findRecorridosEntities(false, maxResults, firstResult);
    }

    private List<Recorridos> findRecorridosEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Recorridos.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Recorridos findRecorridos(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Recorridos.class, id);
        } finally {
            em.close();
        }
    }

    public int getRecorridosCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Recorridos> rt = cq.from(Recorridos.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
