/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package capadao;

import Horario.Horarios;
import controlador.HorariosJpaController;
import controlador.RecorridosJpaController;
import controlador.SociosJpaController;
import java.util.List;
import recorrido.Recorridos;
import socios.Socios;

/**
 *
 * @author StevenJmnz
 */
public class capadao {
  private Horarios hora = new Horarios();
    private HorariosJpaController control = new HorariosJpaController();
    
    
     private Recorridos re = new Recorridos();
    private RecorridosJpaController controlre = new RecorridosJpaController();
    
    
    private Socios so = new Socios();
    private SociosJpaController controlso = new SociosJpaController();
    private String mensaje = "";
    
    
    
    
    public String insertarHorario(String codigo,String descripcion,String tLlegada, String tSalida, int numeroBus ){
       try{
        hora.setId(Integer.BYTES);
        hora.setCodigo(codigo);
        hora.setDescripcion(descripcion);
        hora.setNumeroBus(numeroBus);
        hora.setTLlegada(tLlegada);
        hora.setTSalida(tSalida);
        hora.setEstado(Boolean.TRUE);
        control.create(hora);
        mensaje = "Horario agregado con éxito";
       } catch(Exception e){
           System.out.println("error numero" + e.getMessage());
           mensaje = "Debe de completar el formulario en su totalidad";
       }
       return mensaje;  
        
    }
    public String actualizarHorario(int id,String descripcion,String tLlegada, String tSalida, boolean estado,int numeroBus){
         try{
        hora.setId(id);
        
        hora.setDescripcion(descripcion);
        hora.setNumeroBus(numeroBus);
        hora.setTLlegada(tLlegada);
        hora.setTSalida(tSalida);
        hora.setEstado(estado);
        control.edit(hora);
        mensaje = "Horario actualizado con éxito";
       } catch(Exception e){
           System.out.println("error numero" + e.getMessage());
           mensaje = "Debe de completar el formulario en su totalidad";
       }
       return mensaje;  
        
    }
    
    public String eliminarHorario(int id){
        
        try {
            control.destroy(id);
            mensaje= "Horario activado con éxito";
        } catch (Exception e) {
            mensaje= "Horario desactivado con éxito";
        }
        return mensaje;
    }  
    
    
    public String buscarHorarioId(int id){
        try{
            control.findbyid(id);
            mensaje="se ha encontrado el horario";
        }catch (Exception e){
            mensaje= "Horario con el código introducido no existe";
        }
      return mensaje;
    }
    
    
    public String InsertarRecorrido(String codigo,String descripcion,String zRecorrido, int cantRecorrido, String tDemora, int numeroBus, boolean estado ){
       try{
        re.setId(Integer.BYTES);
        re.setCodigo(codigo);
        re.setDescripcion(descripcion);
        re.setNumeroBus(0);
        re.setTDemora(tDemora);
        re.setZonaRecorrido(zRecorrido);
        re.setCantRecorrido(cantRecorrido);
        re.setEstado(Boolean.TRUE);
        controlre.create(re);
        mensaje = "Recorrido agregado con éxito";
       } catch(Exception e){
           System.out.println("error numero" + e.getMessage());
           mensaje = "Debe de completar el formulario en su totalidad";
       }
       return mensaje;  
        
    }
    public String actualizarRecorrido(int id,String codigo,String descripcion,String zRecorrido, int cantRecorrido, String tDemora, int numeroBus, boolean estado ){
        try{
        re.setId(id);
        re.setCodigo(codigo);
        re.setDescripcion(descripcion);
        re.setNumeroBus(numeroBus);
        re.setTDemora(tDemora);
        re.setZonaRecorrido(zRecorrido);
        re.setCantRecorrido(cantRecorrido);
        re.setEstado(Boolean.TRUE);
        controlre.edit(re);
        mensaje = "Recorrido actualizado con éxito";
       } catch(Exception e){
           System.out.println("error numero" + e.getMessage());
           mensaje = "Debe de completar el formulario en su totalidad";
       }
       return mensaje;  
}
     public String eliminarRecorrido(int id){
        
        try {
            controlre.destroy(id);
            mensaje= "Recorrido desactivado con éxito";
        } catch (Exception e) {
            mensaje= "Recorrido activado con éxito";
        }
        return mensaje;
    }  
     
     public String buscarRecorridoId(int id){
        try{
            controlre.findbyid(id);
            mensaje="se ha encontrado el horario";
        }catch (Exception e){
            mensaje= "Recorrido con el código introducido no existe";
        }
      return mensaje;
    }
     public String insertarSocios(String codigo,String nombre,String apellidos, String linea , int numeroBus, int capacidad,String ayudante, String chofer){
       try{
        so.setId(Integer.BYTES);
        so.setCodigo(codigo);
        so.setNombre(nombre);
        so.setNumeroBus(numeroBus);
        so.setApellidos(apellidos);
        so.setLineaSocio(linea);
        so.setCapacidad(capacidad);
        so.setAyudante(ayudante);
        so.setChofer(chofer);
        so.setEstado(Boolean.TRUE);
        controlso.create(so);
        mensaje = "Socio agregado con éxito";
       } catch(Exception e){
           System.out.println("error numero" + e.getMessage());
           mensaje = "Socio no agregado";
       }
       return mensaje;  
        
    }
    }

