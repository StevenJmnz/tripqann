/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package recorrido;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author StevenJmnz
 */
@Entity
@Table(name = "recorridos")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Recorridos.findAll", query = "SELECT r FROM Recorridos r"),
    @NamedQuery(name = "Recorridos.findById", query = "SELECT r FROM Recorridos r WHERE r.id = :id"),
    @NamedQuery(name = "Recorridos.findByCodigo", query = "SELECT r FROM Recorridos r WHERE r.codigo = :codigo"),
    @NamedQuery(name = "Recorridos.findByNumeroBus", query = "SELECT r FROM Recorridos r WHERE r.numeroBus = :numeroBus"),
    @NamedQuery(name = "Recorridos.findByDescripcion", query = "SELECT r FROM Recorridos r WHERE r.descripcion = :descripcion"),
    @NamedQuery(name = "Recorridos.findByTDemora", query = "SELECT r FROM Recorridos r WHERE r.tDemora = :tDemora"),
    @NamedQuery(name = "Recorridos.findByZonaRecorrido", query = "SELECT r FROM Recorridos r WHERE r.zonaRecorrido = :zonaRecorrido"),
    @NamedQuery(name = "Recorridos.findByCantRecorrido", query = "SELECT r FROM Recorridos r WHERE r.cantRecorrido = :cantRecorrido"),
    @NamedQuery(name = "Recorridos.findByEstado", query = "SELECT r FROM Recorridos r WHERE r.estado = :estado")})
public class Recorridos implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "codigo")
    private String codigo;
    @Basic(optional = false)
    @Column(name = "numeroBus")
    private int numeroBus;
    @Basic(optional = false)
    @Column(name = "descripcion")
    private String descripcion;
    @Basic(optional = false)
    @Column(name = "tDemora")
    private String tDemora;
    @Basic(optional = false)
    @Column(name = "zonaRecorrido")
    private String zonaRecorrido;
    @Basic(optional = false)
    @Column(name = "cantRecorrido")
    private int cantRecorrido;
    @Column(name = "estado")
    private Boolean estado;

    public Recorridos() {
    }

    public Recorridos(Integer id) {
        this.id = id;
    }

    public Recorridos(Integer id, String codigo, int numeroBus, String descripcion, String tDemora, String zonaRecorrido, int cantRecorrido) {
        this.id = id;
        this.codigo = codigo;
        this.numeroBus = numeroBus;
        this.descripcion = descripcion;
        this.tDemora = tDemora;
        this.zonaRecorrido = zonaRecorrido;
        this.cantRecorrido = cantRecorrido;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public int getNumeroBus() {
        return numeroBus;
    }

    public void setNumeroBus(int numeroBus) {
        this.numeroBus = numeroBus;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getTDemora() {
        return tDemora;
    }

    public void setTDemora(String tDemora) {
        this.tDemora = tDemora;
    }

    public String getZonaRecorrido() {
        return zonaRecorrido;
    }

    public void setZonaRecorrido(String zonaRecorrido) {
        this.zonaRecorrido = zonaRecorrido;
    }

    public int getCantRecorrido() {
        return cantRecorrido;
    }

    public void setCantRecorrido(int cantRecorrido) {
        this.cantRecorrido = cantRecorrido;
    }

    public Boolean getEstado() {
        return estado;
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Recorridos)) {
            return false;
        }
        Recorridos other = (Recorridos) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "recorrido.Recorridos[ id=" + id + " ]";
    }
    
}
