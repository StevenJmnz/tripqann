/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import capadao.capadao;
import org.eclipse.persistence.jpa.jpql.Assert;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author StevenJmnz
 */
public class tddTest {
    
    capadao hdao = new capadao();
    // El metodo debe de retornar el value.
    @Test
    public void crearSocioValido() {

        String value = "Socio agregado con éxito";

        String codigo = "S001";
        String nombre = "Pepe";
        String apellidos = "Herrera Jimenez";
        String lineaSocio = "San Jose";
        int numeroBus = 5678;
        int capacidad = 50;
        String ayudante = "Steve Araya";
        String chofer =  "Steven Jimenez";
        

        String resultado = hdao.insertarSocios(codigo, nombre, apellidos,  lineaSocio ,  numeroBus,  capacidad, ayudante,  chofer);

        Assert.assertEquals(value, resultado);

    }
    
    // El metodo debe de retornar el value.
    @Test
    public void crearSocioNoValido() {

        String value = "Socio no agregado";

        String codigo = "S001";
        String nombre = "Pepe";
        String apellidos = "Herrera Jimenez";
        String lineaSocio = "San Jose";
        int numeroBus = 5678;
        int capacidad = 50;
        String ayudante = "Steve Araya";
        String chofer =  "Steven Jimenez";
        

        String resultado = hdao.insertarSocios(codigo, nombre, apellidos, lineaSocio, numeroBus, capacidad, ayudante, chofer);

        Assert.assertEquals(value, resultado);

    }
}
