/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import capadao.capadao;
import org.eclipse.persistence.jpa.jpql.Assert;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;


/**
 *
 * @author StevenJmnz
 */
public class horario {
    
    capadao hdao = new capadao();
        //metodos de insersión de horarios
    
    // El metodo debe de retornar el value.
    @Test
    public void crearHorarioValido() {

        String value = "Horario agregado con éxito";

        String codigo = "H001";
        int numeroBus = 5678;
        String descripcion = "Bus de la  empresa A";
        String tSalida = "8:00am";
        String tLlegada = "8:30am";
        int estado =  1;

        String resultado = hdao.insertarHorario(codigo,descripcion,tLlegada,tSalida,numeroBus);

        Assert.assertEquals(value, resultado);

    }

    // El metodo debe de retornar el value.
    @Test
    public void crearHorarioNoValido() {

        String value = "Debe de completar el formulario en su totalidad";

        String codigo = "H001";
        int numeroBus = 0;
        String descripcion = "Bus de la  empresa A";
        String tSalida = "8:00am";
        String tLlegada = "8:30am";
        int estado =  1;

        String resultado = hdao.insertarHorario(codigo,descripcion,tLlegada,tSalida,numeroBus);

        Assert.assertEquals(value, resultado);

    }

    // En este se debe de validar que no venga vacio el objeto que trae. 
    // El objeto de horario no se si esta bien creada la instancia. 
    @Test
    public void buscarHorarioValido() {

        int id =1;
        String resultado = hdao.buscarHorarioId(id);

        Assert.assertNotNull(resultado);

    }

    // El metodo debe de retornar el value.
    @Test
    public void buscarHorarioNoValido() {

        String value = "Horario con el código introducido no existe";

       int id =1;
        String resultado = hdao.buscarHorarioId(id);

        Assert.assertEquals(value, resultado);

    }

    // Este metodo primero tiene que ir a consultar el metodo de buscar horario, despues si lo encuentra debe de realizar la 
    // actualización del horario.
    @Test
    public void actualizarHorarioValido() {

        String value = "Horario actualizado con éxito";

        
        
        int id = 5678;
        String descripcion = "Bus de la  empresa Empresarios del Norte";
        String tSalida = "8:00am";
        String tLlegada = "8:30am";
        boolean estado = true;
        int numeroBus =  1;

        String resultado = hdao.actualizarHorario( id, descripcion, tLlegada, tSalida,estado, numeroBus);

        Assert.assertEquals(value, resultado);

    }

    // El metodo debe de retornar el value. A la hora de ir un dato null no se debe de insertar en la base de datos
    // y debe de reotnar el value.
    @Test
    public void actualizarHorarioNoValido() {

        String value = "Debe de completar el formulario en su totalidad";

     
        
        int id = 5678;
        String descripcion = "Bus de la  empresa Empresarios del Norte";
        String tSalida = "8:00am";
        String tLlegada = "8:30am";
        boolean estado = true;
        int numeroBus =  1;

        String resultado = hdao.actualizarHorario( id, descripcion, tLlegada, tSalida,estado, numeroBus);

        Assert.assertEquals(value, resultado);

    }

    // Este metodo primero tiene que ir a consultar el metodo de buscar horario, despues si lo encuentra debe de realizar la 
    // desactivación del horario.
    @Test
    public void desactivarHorario() {

        String value = "Horario desactivado con éxito";

        int id = 1;

        String resultado = hdao.eliminarHorario(id);

        Assert.assertEquals(value, resultado);

    }

    // Este metodo primero tiene que ir a consultar el metodo de buscar horario, despues si lo encuentra debe de realizar la 
    // activación del horario.
    @Test
    public void activarHorario() {

        String value = "Horario activado con éxito";

           int id = 1;

        String resultado = hdao.eliminarHorario(id);

        Assert.assertEquals(value, resultado);

    }

    // Este metodo primero tiene que ir a consultar el metodo de buscar horario, despues si lo encuentra debe de realizar la 
    // busqueda de la unidad para generar el reporte.
    // este la verdad no se si va a funcionar pero la idea es ver la longitud del arreglo y validarlo.
    @Test
    public void reporteValido() {

        String codigo = "H001";
        String numeroBus = "5678";

        int esperado = 3;

        Horario obj[] = new Horario[];
        obj[].setData(horario.reporte(codigo, numeroBus))

        Assert.assertArrayEquals(esperado, long[] obj[])

    }

    // Este metodo primero tiene que ir a consultar el metodo de buscar horario, despues si lo encuentra debe de realizar la 
    // busqueda de la unidad para generar el reporte, si la unidad va vacia se genera el error.
    @Test
    public void reporteNoValido() {

        int value = "Datos para generar reporte imcompletos";

        String codigo = "H001";
        String numeroBus = null;

        String resultado = horario.reporte(codigo, numeroBus);

        Assert.assertEquals(value, resultado);

    }
     }

