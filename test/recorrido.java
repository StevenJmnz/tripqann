/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import capadao.capadao;
import org.eclipse.persistence.jpa.jpql.Assert;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author StevenJmnz
 */
public class recorrido {
    
     capadao hdao = new capadao();
    // El metodo debe de retornar el value.
    @Test
    public void crearRecorridoValido() {

        
        String value = "Recorrido agregado con éxito";

        String codigo = "R001";
        int numeroBus = 5678;
        String descripcion = "Recorrido semanal";
        String tDemora = "2 horas";
        String zonaRecorrido = "San Jose Centro";
        int cantRecorrido = 1;
        boolean estado =  true;

        String resultado = hdao.InsertarRecorrido(  codigo, descripcion, zonaRecorrido,  cantRecorrido,  tDemora,  numeroBus,  estado  );

        Assert.assertEquals(value, resultado);

    }
    
    // El metodo debe de retornar el value.
    @Test
    public void crearRecorridoNoValido() {

        String value = "Debe de completar el formulario en su totalidad";

        
       
        String codigo = "R001";
        int numeroBus = 5678;
        String descripcion = "Recorrido semanal";
        String tDemora = "2 horas";
        String zonaRecorrido = "San Jose Centro";
        int cantRecorrido = 1;
        boolean estado =  true;

        String resultado = hdao.InsertarRecorrido(  codigo, descripcion, zonaRecorrido,  cantRecorrido,  tDemora,  numeroBus,  estado  );

        Assert.assertEquals(value, resultado);

    }

    // En este se debe de validar que no venga vacio el objeto que trae. 
    // El objeto de recorrido no se si esta bien creada la instancia. 
    @Test
    public void buscarRecorridoValido() {

        int id = 1;

        
         String resultado = hdao.buscarRecorridoId(id);

        Assert.assertNotNull(resultado);

    }

    // El metodo debe de retornar el value.
    @Test
    public void buscarRecorridoNoValido() {

        String value = "Recorrido con el código introducido no existe";

         int id = 1;

        
         String resultado = hdao.buscarRecorridoId(id);

        Assert.assertEquals(value, resultado);

    }

    // Este metodo primero tiene que ir a consultar el metodo de buscar recorrido, despues si lo encuentra debe de realizar la 
    // actualización del recorrido.
    @Test
    public void actualizarRecorridoValido() {

        String value = "Recorrido actualizado con éxito";

        String codigo = "R001";
        int id=1;
        int numeroBus = 5678;
        String descripcion = "Recorrido semanal, para empresarios del Norte";
        String tDemora = "2 horas";
        String zonaRecorrido = "San Jose Centro";
        int cantRecorrido = 5;
        boolean estado =  true;

        String resultado = hdao.actualizarRecorrido(id, codigo, descripcion, zonaRecorrido,  cantRecorrido,  tDemora,  numeroBus,  estado );

        Assert.assertEquals(value, resultado);

    }

    // El metodo debe de retornar el value. A la hora de ir un dato null no se debe de insertar en la base de datos
    // y debe de reotnar el value.
    @Test
    public void actualizarRecorridoNoValido() {

        String value = "Debe de completar el formulario en su totalidad";

        String codigo = "R001";
        int id= 1;
        int numeroBus = 5678;
        String descripcion = "Recorrido semanal, para empresarios del Norte";
        String tDemora = "2 horas";
        String zonaRecorrido = null;
        int cantRecorrido = 5;
        boolean estado =  true;

        String resultado = hdao.actualizarRecorrido(id, codigo, descripcion, zonaRecorrido,  cantRecorrido,  tDemora,  numeroBus,  estado);

        Assert.assertEquals(value, resultado);

    }

    // Este metodo primero tiene que ir a consultar el metodo de buscar recorrido, despues si lo encuentra debe de realizar la 
    // desactivación del recorrido.
    @Test
    public void desactivarRecorrido() {

        String value = "Recorrido desactivado con éxito";

        int id= 1;

        String resultado = hdao.eliminarRecorrido(id);

        Assert.assertEquals(value, resultado);

    }

    // Este metodo primero tiene que ir a consultar el metodo de buscar recorrido, despues si lo encuentra debe de realizar la 
    // activación del recorrido.
    @Test
    public void activarRecorrido() {

        String value = "Recorrido activado con éxito";

        int id= 1;

        String resultado = hdao.eliminarRecorrido(id);

        Assert.assertEquals(value, resultado);

    }


    // Este metodo primero tiene que ir a consultar el metodo de buscar recorrido, despues si lo encuentra debe de realizar la 
    // busqueda de la unidad para generar el reporte.
    // este la verdad no se si va a funcionar pero la idea es ver la longitud del arreglo y validarlo.
    @Test
    public void reporteValido() {

        String codigo = "R001";
        String numeroBus = "5678";

        int esperado = 3;

        Recorrido obj[] = new Recorrido[];
        obj[].setData(recorrido.reporte(codigo, numeroBus))

        Assert.assertArrayEquals(esperado, long[] obj[])

    }

    // Este metodo primero tiene que ir a consultar el metodo de buscar recorrido, despues si lo encuentra debe de realizar la 
    // busqueda de la unidad para generar el reporte, si la unidad va vacia se genera el error.
    @Test
    public void reporteNoValido() {

        int value = "Datos para generar reporte imcompletos";

        String codigo = "R001";
        String numeroBus = null;

        String resultado = recorrido.reporte(codigo, numeroBus);

        Assert.assertEquals(value, resultado);

    }

}
